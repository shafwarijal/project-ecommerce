"use client";

import React, { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";

import { CartItem } from "./types";

import classes from "./style.module.scss";

const Navbar = () => {
  const [open, setOpen] = useState(false);
  const [openCart, setOpenCart] = useState(false);
  const [cartItems, setCartItems] = useState<CartItem[]>([]);

  const getCart = () => {
    const cart = JSON.parse(localStorage.getItem("cartLocal") || "[]");
    // console.log(cartItems);

    setCartItems(cart);
  };

  const removeCartItem = (index: number) => {
    const storedCart = JSON.parse(localStorage.getItem("cartLocal") || "[]");

    if (index >= 0 && index < storedCart.length) {
      storedCart.splice(index, 1);
      localStorage.setItem("cartLocal", JSON.stringify(storedCart));
      setCartItems(storedCart); // Update the state to reflect the change
    }
  };

  useEffect(() => {
    getCart();
  }, []);

  return (
    <div className={classes.navbar}>
      <div className={classes.container}>
        <div className={`${classes.btnMobile}`} onClick={() => setOpen(!open)}>
          <Image
            src="/assets/images/icon-menu.svg"
            alt="cart"
            width={16}
            height={15}
            priority
          />
        </div>
        <div className={classes.desktop}>
          <div className={classes.left}>
            <div className={classes.logo}>sneakers</div>
            <div className={classes.menu}>
              <ul className={classes.list}>
                <li className={classes.item}>
                  <Link href="">Collection</Link>
                </li>
                <li className={classes.item}>
                  <Link href="">Men</Link>
                </li>
                <li className={classes.item}>
                  <Link href="">Women</Link>
                </li>
                <li className={classes.item}>
                  <Link href="">About</Link>
                </li>
                <li className={classes.item}>
                  <Link href="">Contact</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className={classes.right}>
            <div
              className={classes.cart}
              onClick={() => {
                getCart();
                setOpenCart(!openCart);
              }}
            >
              <Link href="">
                <Image
                  src="/assets/icons/icon-cart-1.svg"
                  alt="cart"
                  width={22}
                  height={20}
                  priority
                />
              </Link>
            </div>
            <div className={classes.avatar}>
              <Link href="">
                <Image
                  src="/assets/images/image-avatar-1.png"
                  alt="avatar"
                  layout="fill"
                  className={classes.avatarImage}
                  priority
                />
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className={`${classes.mobile}  ${open && classes.active}`}>
        <div className={classes.containerMobile}>
          <div className={classes.iconClose} onClick={() => setOpen(!open)}>
            <Image
              src="/assets/images/icon-close.svg"
              alt="close"
              width={16}
              height={16}
              priority
            ></Image>
          </div>
          <div className={classes.menuMobile}>
            <ul className={classes.listMobile}>
              <li className={classes.itemMobile}>
                <Link href="">Collection</Link>
              </li>
              <li className={classes.itemMobile}>
                <Link href="">Men</Link>
              </li>
              <li className={classes.itemMobile}>
                <Link href="">Women</Link>
              </li>
              <li className={classes.itemMobile}>
                <Link href="">About</Link>
              </li>
              <li className={classes.itemMobile}>
                <Link href="">Contact</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className={`${classes.modalCart} ${openCart && classes.activeCart}`}>
        <div className={classes.containerCart}>
          <div className={classes.title}>Cart</div>

          {cartItems.length === 0 ? (
            <div className={classes.empty}>Your cart is empty.</div>
          ) : (
            <>
              <div className={classes.cartItems}>
                {cartItems?.map((item, index) => (
                  <div key={index} className={classes.cartItem}>
                    <div className={classes.cartItemLeft}>
                      <div className={classes.cartItemImage}>
                        <Image
                          src={item.image}
                          alt="product"
                          className={classes.cartItemImageClasses}
                          priority
                        />
                      </div>
                      <div className={classes.cartItemInfo}>
                        <div className={classes.cartItemInfoName}>
                          {item.name}
                        </div>
                        <div className={classes.cartItemInfoPrice}>
                          <div className={classes.cartItemInfoPriceQuantity}>
                            {`$${item.price} x ${item.quantity}`}
                          </div>
                          <div className={classes.cartItemInfoTotal}>
                            {`$${item.price * item.quantity}`}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={classes.cartItemRight}>
                      <div
                        className={classes.deleteIcon}
                        onClick={() => {
                          removeCartItem(index);
                          getCart();
                        }}
                      >
                        <Image
                          src="/assets/images/icon-delete.svg"
                          alt="delete"
                          layout="fill"
                          className={classes.deleteIconImage}
                          priority
                        />
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className={classes.checkout}>
                <div className={classes.btnCheckout}>Checkout</div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Navbar;

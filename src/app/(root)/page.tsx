"use client";

import React, { useState, useEffect } from "react";
import Image from "next/image";

import productItem1 from "../../../public/assets/images/image-product-1.jpg";
import productItem2 from "../../../public/assets/images/image-product-2.jpg";
import productItem3 from "../../../public/assets/images/image-product-3.jpg";
import productItem4 from "../../../public/assets/images/image-product-4.jpg";
import productItemThumbnail1 from "../../../public/assets/images/image-product-1-thumbnail.jpg";
import productItemThumbnail2 from "../../../public/assets/images/image-product-2-thumbnail.jpg";
import productItemThumbnail3 from "../../../public/assets/images/image-product-3-thumbnail.jpg";
import productItemThumbnail4 from "../../../public/assets/images/image-product-4-thumbnail.jpg";
import proviousImage from "../../../public/assets/images/icon-previous.svg";
import nextImage from "../../../public/assets/images/icon-next.svg";
import closeModal from "../../../public/assets/images/icon-close-modal.svg";

import { CartItem } from "../../components/Navbar/types";

import classes from "./style.module.scss";

const Home = () => {
  const productList = [productItem1, productItem2, productItem3, productItem4];

  const [quantity, setQuantity] = useState(0);
  const [preview, setPreview] = useState(0);
  const [cart, setCart] = useState<any>([]);
  const [openModal, setOpenModal] = useState(false);

  const products = productList.map((item, index) => {
    return (
      <div className={classes.preview} key={index}>
        <Image
          src={item}
          alt="product"
          className={classes.previewImage}
          priority
        ></Image>
      </div>
    );
  });

  const hanldePreview = (index: number) => {
    setPreview(index);
  };

  const handleDecrement = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handlePrevious = () => {
    if (preview > 0) {
      setPreview(preview - 1);
    }
  };

  const handleNext = () => {
    if (preview < productList.length - 1) {
      setPreview(preview + 1);
    }
  };

  const addCart = () => {
    if (quantity === 0) return;

    const newCart = {
      image: productList[preview],
      name: "Fall Limited Edition Sneakers",
      price: 125,
      quantity: quantity,
    };

    setCart((prevCart: CartItem[]) => [...prevCart, newCart]);
    setQuantity(0);
  };

  useEffect(() => {
    const storedCart = localStorage.getItem("cartLocal");

    if (storedCart) {
      setCart(JSON.parse(storedCart));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("cartLocal", JSON.stringify(cart));
  }, [cart]);

  return (
    <>
      <div className={classes.home}>
        <div className={classes.container}>
          <div className={classes.contentLeft}>
            <div
              className={classes.imagePreviewBox}
              onClick={() => setOpenModal(true)}
            >
              {products[preview]}
            </div>

            <div className={classes.listImageProduct}>
              <div className={classes.listImageProductItem}>
                <Image
                  src={productItemThumbnail1}
                  alt="Images 1"
                  className={`${classes.listImage} ${
                    preview === 0 ? classes.active : ""
                  }`}
                  onClick={() => hanldePreview(0)}
                ></Image>
              </div>
              <div className={classes.listImageProductItem}>
                <Image
                  src={productItemThumbnail2}
                  alt="Images 2"
                  className={`${classes.listImage} ${
                    preview === 1 ? classes.active : ""
                  }`}
                  onClick={() => hanldePreview(1)}
                ></Image>
              </div>
              <div className={classes.listImageProductItem}>
                <Image
                  src={productItemThumbnail3}
                  alt="Images 3"
                  className={`${classes.listImage} ${
                    preview === 2 ? classes.active : ""
                  }`}
                  onClick={() => hanldePreview(2)}
                ></Image>
              </div>
              <div className={classes.listImageProductItem}>
                <Image
                  src={productItemThumbnail4}
                  alt="Images 4"
                  className={`${classes.listImage} ${
                    preview === 3 ? classes.active : ""
                  }`}
                  onClick={() => hanldePreview(3)}
                ></Image>
              </div>
            </div>
          </div>
          <div className={classes.contentRight}>
            <div className={classes.title}>Sneaker Company</div>
            <div className={classes.subTitle}>
              Fall Limited Edition Sneakers
            </div>
            <div className={classes.text}>
              These low-profile sneakers are your perfect casual wear companion.
              Featuring a durable rubber outer sole, they’ll withstand
              everything the weather can offer.
            </div>
            <div className={classes.priceBox}>
              <div className={classes.price}>
                <div className={classes.titlePrice}>$125.00</div>
                <div className={classes.titleDiscount}>50%</div>
              </div>
              <div className={classes.discount}>$250.00</div>
            </div>
            <div className={classes.cart}>
              <div className={classes.countCart}>
                <div className={classes.decrement} onClick={handleDecrement}>
                  <Image
                    src="/assets/images/icon-minus.svg"
                    alt="minus"
                    layout="fill"
                    className={classes.decrementImage}
                  ></Image>
                </div>
                <div className={classes.titleCount}>{quantity}</div>
                <div className={classes.increment} onClick={handleIncrement}>
                  <Image
                    src="/assets/images/icon-plus.svg"
                    alt="plus"
                    layout="fill"
                    className={classes.incrementImage}
                  ></Image>
                </div>
              </div>
              <div className={classes.addCart} onClick={addCart}>
                <div className={classes.iconAddCart}>
                  <Image
                    src="/assets/images/icon-cart.svg"
                    alt="cart"
                    layout="fill"
                    className={classes.iconAddCartImage}
                  ></Image>
                </div>
                <div className={classes.titleCart}>Add to cart</div>
              </div>
            </div>
          </div>
        </div>
        {openModal && (
          <div className={classes.modalImage}>
            <div className={classes.modalContainer}>
              <div className={classes.boxCloseModal}>
                <div
                  className={classes.closeModal}
                  onClick={() => setOpenModal(false)}
                >
                  <Image
                    src={closeModal}
                    alt="close"
                    layout="fill"
                    className={classes.closeImage}
                  ></Image>
                </div>
              </div>

              <div className={classes.previewModal}>
                {products[preview]}
                <div className={classes.previous} onClick={handlePrevious}>
                  <Image
                    src={proviousImage}
                    alt="previous"
                    layout="fill"
                    className={classes.previousImage}
                  ></Image>
                </div>
                <div className={classes.next} onClick={handleNext}>
                  <Image
                    src={nextImage}
                    alt="next"
                    layout="fill"
                    className={classes.nextImage}
                  ></Image>
                </div>
              </div>
              <div className={classes.thumbnailModal}>
                <div className={classes.listImageProductItem}>
                  <Image
                    src={productItemThumbnail1}
                    alt="Images 1"
                    className={`${classes.listImage} ${
                      preview === 0 ? classes.active : ""
                    }`}
                    onClick={() => hanldePreview(0)}
                  ></Image>
                </div>
                <div className={classes.listImageProductItem}>
                  <Image
                    src={productItemThumbnail2}
                    alt="Images 2"
                    className={`${classes.listImage} ${
                      preview === 1 ? classes.active : ""
                    }`}
                    onClick={() => hanldePreview(1)}
                  ></Image>
                </div>
                <div className={classes.listImageProductItem}>
                  <Image
                    src={productItemThumbnail3}
                    alt="Images 3"
                    className={`${classes.listImage} ${
                      preview === 2 ? classes.active : ""
                    }`}
                    onClick={() => hanldePreview(2)}
                  ></Image>
                </div>
                <div className={classes.listImageProductItem}>
                  <Image
                    src={productItemThumbnail4}
                    alt="Images 4"
                    className={`${classes.listImage} ${
                      preview === 3 ? classes.active : ""
                    }`}
                    onClick={() => hanldePreview(3)}
                  ></Image>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Home;
